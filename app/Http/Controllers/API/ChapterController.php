<?php

namespace App\Http\Controllers\API;

use App\Models\Course;
use App\Models\Chapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ChapterController extends Controller
{
    public function index(Request $request) {
        $chapter = Chapter::query();
        $courseId = $request->query('course_id');

        $chapter->when($courseId, function($query) use ($courseId) {
            return $query->where('course_id', '=', $courseId);
        });

        $chapter = $chapter->get();

        return $this->ResponeSuccess('success get all chapter', $chapter);
    }

    public function show($id) {
        $chapter = Chapter::find($id);

        if(!$chapter) {
            return $this->ValidationError('Chapter Not Found');
        }

        return $this->ResponeSuccess('success get chapter', $chapter);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'      => 'required|string',
            'course_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $course = Course::find($request->course_id);

            if(!$course) {
                return $this->ValidationError('Course Not Found');
            }

            $chapter = Chapter::create([
                'name'      => $request->name,
                'course_id' => $request->course_id
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store chapter', $chapter);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name'      => 'string',
            'course_id' => 'integer'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $chapter = Chapter::find($id);

            if(!$chapter) {
                return $this->ValidationError('Chapter Not Found');
            }

            $course = Course::find($request->course_id);

            if(!$course) {
                return $this->ValidationError('Course Not Found');
            }

            $chapter->update([
                'name'      => $request->name,
                'course_id' => $request->course_id
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success updated course', $course);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $chapter = Chapter::find($id);

        if(!$chapter) {
            return $this->ValidationError('Chapter Not Found');
        }

        try {
            DB::beginTransaction();

            $chapter->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy chapter', $chapter);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
