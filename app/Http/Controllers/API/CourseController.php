<?php

namespace App\Http\Controllers\API;

use App\Models\Course;
use App\Models\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Chapter;
use App\Models\MyCourse;
use App\Models\Review;
use Illuminate\Support\Facades\Validator;

class CourseController extends Controller
{
    public function index(Request $request) {
        $courses = Course::query();

        $keyword = $request->query('keyword');
        $status = $request->query('status');

        $courses->when($keyword, function($query) use ($keyword) {
            return $query->where('name', 'LIKE', '%' . $keyword . '%');
        });

        $courses->when($status, function($query) use ($status) {
            return $query->where('status', '=', $status);
        });

        $courses = $courses->paginate(10);

        return $this->ResponeSuccess('success get all courses', $courses);
    }

    public function show($id) {
        $course = Course::with('chapters.lessons')->with('mentor')->with('images')->find($id);

        if(!$course) {
            return $this->ValidationError('Course Not Found');
        }

        $reviews = Review::where('course_id', '=', $id)->get()->toArray();
        if(count($reviews) > 0) {
            $userIds = array_column($reviews, 'user_id');
            $users = getUserByIds($userIds);
            if($users['status'] === 'error') {
                $reviews = [];
            } else {
                foreach($reviews as $key => $review) {
                    $userIndex = array_search($review['user_id'], array_column($users['data'], 'id'));
                    $reviews[$key]['users'] = $users['data'][$userIndex];
                }
            }
        }

        $totalStudent = MyCourse::where('course_id', '=', $id)->count();
        $totalVideos = Chapter::where('course_id', '=', $id)->withCount('lessons')->get()->toArray();
        $finalTotalVideos = array_sum(array_column($totalVideos, 'lesson_count'));

        $course['reviews'] = $reviews;
        $course['total_videos'] = $finalTotalVideos;
        $course['total_student'] = $totalStudent;

        return $this->ResponeSuccess('success get course', $course);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'        => 'required|string',
            'certificate' => 'required|boolean',
            'thumbnail'   => 'string|url',
            'type'        => 'required|in:free,premium',
            'status'      => 'required|in:draft,published',
            'price'       => 'integer',
            'level'       => 'required|in:all-level,beginner,intermediate,advance',
            'mentor_id'   => 'required|integer',
            'description' => 'string'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $mentor = Mentor::find($request->mentor_id);

            if(!$mentor) {
                return $this->ValidationError('Mentor Not Found');
            }

            $course = Course::create([
                'name'        => $request->name,
                'certificate' => $request->certificate,
                'thumbnail'   => $request->thumbnail,
                'type'        => $request->type,
                'status'      => $request->status,
                'price'       => $request->price,
                'level'       => $request->level,
                'mentor_id'   => $request->mentor_id,
                'description' => $request->description,
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store course', $course);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name'        => 'string',
            'certificate' => 'boolean',
            'thumbnail'   => 'string|url',
            'type'        => 'in:free,premium',
            'status'      => 'in:draft,published',
            'price'       => 'integer',
            'level'       => 'in:all-level,beginner,intermediate,advance',
            'mentor_id'   => 'integer',
            'description' => 'string'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $course = Course::find($id);

            if(!$course) {
                return $this->ValidationError('Mentor Not Found');
            }

            $mentor = Mentor::find($request->mentor_id);

            if(!$mentor) {
                return $this->ValidationError('Mentor Not Found');
            }

            $course->update([
                'name'        => $request->name,
                'certificate' => $request->certificate,
                'thumbnail'   => $request->thumbnail,
                'type'        => $request->type,
                'status'      => $request->status,
                'price'       => $request->price,
                'level'       => $request->level,
                'mentor_id'   => $request->mentor_id,
                'description' => $request->description,
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success updated course', $course);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $course = Course::find($id);

        if(!$course) {
            return $this->ValidationError('Course Not Found');
        }

        try {
            DB::beginTransaction();

            $course->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy course', $course);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
