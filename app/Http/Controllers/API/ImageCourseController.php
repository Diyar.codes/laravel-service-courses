<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\ImageCourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ImageCourseController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'image'     => 'required|url',
            'course_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $course = Course::find($request->course_id);

            if(!$course) {
                return $this->ValidationError('Course Not Found');
            }

            $imageCourse = ImageCourse::create([
                'image'       => $request->image,
                'course_id' => $request->course_id
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store Image Course', $imageCourse);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $imageCourse = ImageCourse::find($id);

        if(!$imageCourse) {
            return $this->ValidationError('Image Course Not Found');
        }

        try {
            DB::beginTransaction();

            $imageCourse->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy Image Course', $imageCourse);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
