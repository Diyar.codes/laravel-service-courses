<?php

namespace App\Http\Controllers\API;

use App\Models\Lesson;
use App\Models\Chapter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class LessonController extends Controller
{
    public function index(Request $request) {
        $lesson = Lesson::query();

        $chapterId = $request->query('chapter_id');

        $lesson->when($chapterId, function($query) use ($chapterId) {
            return $query->where('chapter_id', '=', $chapterId);
        });

        $lesson = $lesson->get();

        return $this->ResponeSuccess('success get all chapter', $lesson);
    }

    public function show($id) {
        $lesson = Lesson::find($id);

        if(!$lesson) {
            return $this->ValidationError('Lesson Not Found');
        }

        return $this->ResponeSuccess('success get lesson', $lesson);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'       => 'required|string',
            'video'      => 'required|string',
            'chapter_id' => 'required|integer'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $chapter = Chapter::find($request->chapter_id);

            if(!$chapter) {
                return $this->ValidationError('Chapter Not Found');
            }

            $lesson = Lesson::create([
                'name'       => $request->name,
                'video'      => $request->video,
                'chapter_id' => $request->chapter_id,
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store lesson', $lesson);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name'       => 'string',
            'video'      => 'string',
            'chapter_id' => 'integer'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $lesson = Lesson::find($id);

            if(!$lesson) {
                return $this->ValidationError('Lesson Not Found');
            }

            $chapter = Chapter::find($request->chapter_id);

            if(!$chapter) {
                return $this->ValidationError('Chapter Not Found');
            }

            $lesson->update([
                'name'       => $request->name,
                'video'      => $request->video,
                'chapter_id' => $request->chapter_id
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success updated lesson', $lesson);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $lesson = Lesson::find($id);

        if(!$lesson) {
            return $this->ValidationError('lesson Not Found');
        }

        try {
            DB::beginTransaction();

            $lesson->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy lesson', $lesson);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
