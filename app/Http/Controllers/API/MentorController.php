<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Mentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class MentorController extends Controller
{
    public function index() {
        $mentor = Mentor::all();

        return $this->ResponeSuccess('success get all mentor', $mentor);
    }

    public function show($id) {
        $mentor = Mentor::find($id);

        if(!$mentor) {
            return $this->ValidationError('Mentor Not Found');
        }

        return $this->ResponeSuccess('success get mentor', $mentor);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'       => 'required|string',
            'profile'    => 'required|url',
            'profession' => 'required|string',
            'email'      => 'required|email',
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $mentor = Mentor::create([
                'name'       => $request->name,
                'profile'    => $request->profile,
                'profession' => $request->profession,
                'email'      => $request->email
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store mentor', $mentor);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'name'       => 'string',
            'profile'    => 'url',
            'profession' => 'string',
            'email'      => 'email',
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $mentor = Mentor::find($id);

            if(!$mentor) {
                return $this->ValidationError('Mentor Not Found');
            }

            $mentor->update([
                'name'       => $request->name,
                'profile'    => $request->profile,
                'profession' => $request->profession,
                'email'      => $request->email
            ]);

            DB::commit();

            return $this->ResponeSuccess('success update mentor', $mentor);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $mentor = Mentor::find($id);

        if(!$mentor) {
            return $this->ValidationError('Mentor Not Found');
        }

        try {
            DB::beginTransaction();

            $mentor->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy mentor', $mentor);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
