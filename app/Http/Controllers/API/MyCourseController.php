<?php

namespace App\Http\Controllers\API;

use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\MyCourse;
use Illuminate\Support\Facades\Validator;

class MyCourseController extends Controller
{
    public function index(Request $request) {
        $myCourses = MyCourse::query()->with('course');

        $userId = $request->query('user_id');
        $myCourses->when($userId, function($query) use ($userId) {
            $query->where('user_id', '=', $userId);
        });

        $myCourses = $myCourses->get();

        return $this->ResponeSuccess('success get all my course', $myCourses);
    }

    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'course_id' => 'required|integer',
            'user_id'   => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $course = Course::find($request->course_id);

            if(!$course) {
                return $this->ValidationError('Course Not Found');
            }

            $user = getUser($request->user_id);

            if($user['status'] === 'error') {
                return response()->json([
                    'status'  => $user['status'],
                    'message' => $user['message']
                ], $user['http_code']);
            }

            $isExistMyCourse = MyCourse::where('course_id', '=', $request->course_id)
                                            ->where('user_id', '=', $request->user_id)
                                            ->exists();

            if($isExistMyCourse) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'user already take this course'
                ], 409);
            }

            if($course->type === 'premium') {
                if($course->price === 0) {
                    return $this->ValidationError('Price Can`t 0');
                }

                $order = postOrder([
                    'user'   => $user['data'],
                    'course' => $course->toArray()
                ]);

                if($order['status'] === 'error') {
                    return response()->json([
                        'status'  => $order['status'],
                        'message' => $order['message']
                    ], $order['http_code']);
                }

                DB::commit();

                return $this->ResponeSuccessStore('success store my course', $order['data']);
            } else {
                $myCourse = MyCourse::create([
                    'course_id' => $request->course_id,
                    'user_id'   => $request->user_id
                ]);

                DB::commit();

                return $this->ResponeSuccessStore('success store my course', $myCourse);
            }

        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function createPremiumAccess(Request $request) {
        $myCourse = MyCourse::create([
            "course_id" => $request->course_id,
            "user_id"   => $request->user_id
        ]);

        return $this->ResponeSuccessStore('success store my course', $myCourse);
    }
}
