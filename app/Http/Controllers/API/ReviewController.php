<?php

namespace App\Http\Controllers\API;

use App\Models\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Support\Facades\Validator;

class ReviewController extends Controller
{
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required|integer',
            'course_id' => 'required|integer',
            'rating'    => 'required|integer|min:1|max:5',
            'note'      => 'string'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $course = Course::find($request->course_id);

            if(!$course) {
                return $this->ValidationError('Course Not Found');
            }

            $user = getUser($request->user_id);

            if($user['status'] === 'error') {
                return response()->json([
                    'status'  => $user['status'],
                    'message' => $user['message']
                ], $user['http_code']);
            }

            $isExistMyReview = Review::where('course_id', '=', $request->course_id)
                                        ->where('user_id', '=', $request->user_id)
                                        ->exists();

            if($isExistMyReview) {
                return response()->json([
                    'status'  => 'error',
                    'message' => 'review already exists'
                ], 409);
            }

            $review = Review::create([
                'user_id'   => $request->user_id,
                'course_id' => $request->course_id,
                'rating'    => $request->rating,
                'note'      => $request->note
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success store review', $review);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function update(Request $request, $id) {
        $validator = Validator::make($request->all(), [
            'rating' => 'integer|min:1|max:5',
            'note'   => 'string'
        ]);

        if ($validator->fails()) {
            return $this->ValidationError($validator->errors());
        }

        try {
            DB::beginTransaction();

            $review = Review::find($id);

            if(!$review) {
                return $this->ValidationError('Review Not Found');
            }

            $review->update([
                'reting' => $request->reting,
                'note'   => $request->note
            ]);

            DB::commit();

            return $this->ResponeSuccessStore('success updated review', $review);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }

    public function destroy($id) {
        $review = Review::find($id);

        if(!$review) {
            return $this->ValidationError('Review Not Found');
        }

        try {
            DB::beginTransaction();

            $review->delete();

            DB::commit();

            return $this->ResponeSuccess('success destroy review', $review);
        } catch (\Exception $e) {
            DB::rollBack();

            return $this->ServerError($e->getMessage());
        }
    }
}
