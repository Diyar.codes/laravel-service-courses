<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\MentorController;
use App\Http\Controllers\API\CourseController;
use App\Http\Controllers\API\ChapterController;
use App\Http\Controllers\API\LessonController;
use App\Http\Controllers\API\ImageCourseController;
use App\Http\Controllers\API\MyCourseController;
use App\Http\Controllers\API\ReviewController;

// mentor
Route::namespace('API')->prefix('v1/mentor')->group(function () {
    Route::get('/', [MentorController::class, 'index']);
    Route::get('/{id}', [MentorController::class, 'show']);
    Route::post('/', [MentorController::class, 'store']);
    Route::put('/{id}', [MentorController::class, 'update']);
    Route::delete('/{id}', [MentorController::class, 'destroy']);
});

// course
Route::namespace('API')->prefix('v1/course')->group(function () {
    Route::get('/', [CourseController::class, 'index']);
    Route::get('/{id}', [CourseController::class, 'show']);
    Route::post('/', [CourseController::class, 'store']);
    Route::put('/{id}', [CourseController::class, 'update']);
    Route::delete('/{id}', [CourseController::class, 'destroy']);
});

// chapter
Route::namespace('API')->prefix('v1/chapter')->group(function () {
    Route::get('/', [ChapterController::class, 'index']);
    Route::get('/{id}', [ChapterController::class, 'show']);
    Route::post('/', [ChapterController::class, 'store']);
    Route::put('/{id}', [ChapterController::class, 'update']);
    Route::delete('/{id}', [ChapterController::class, 'destroy']);
});

// lesson
Route::namespace('API')->prefix('v1/lesson')->group(function () {
    Route::get('/', [LessonController::class, 'index']);
    Route::get('/{id}', [LessonController::class, 'show']);
    Route::post('/', [LessonController::class, 'store']);
    Route::put('/{id}', [LessonController::class, 'update']);
    Route::delete('/{id}', [LessonController::class, 'destroy']);
});

// image course
Route::namespace('API')->prefix('v1/image-course')->group(function () {
    Route::post('/', [ImageCourseController::class, 'store']);
    Route::delete('/{id}', [ImageCourseController::class, 'destroy']);
});

// my course
Route::namespace('API')->prefix('v1/my-courses')->group(function () {
    Route::get('/', [MyCourseController::class, 'index']);
    Route::post('/', [MyCourseController::class, 'store']);
    Route::post('/premium', [MyCourseController::class, 'createPremiumAccess']);
});

// review
Route::namespace('API')->prefix('v1/review')->group(function () {
    Route::post('/', [ReviewController::class, 'store']);
    Route::put('/{id}', [ReviewController::class, 'update']);
    Route::delete('/{id}', [ReviewController::class, 'destroy']);
});
